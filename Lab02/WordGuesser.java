public class WordGuesser{
	//asks user for word and calls teh runGame method
	public static void main(String []args){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("Input a word to guess:");
		runGame(reader.next());
	}
	
	//sees if theres inputted letter in word and sends (first) location of it
	public static int isLetterInWord(String word, char c){	
		//goes through all chars of the word (1-4 or 0-3)
		for (int place = 0; place < word.length(); place++){
			//verifies if inputted char is the same as letter of the given place
			//returns given place and ends method
			if (word.charAt(place) == c) return place;
		}
		//if the inputted char was not in the word method will go here and return -1
		final int invalid = -1;
		return invalid;
	}
	
	public static void printWord(String word, boolean[] lettersRevealed){
		String answer = "";
		//array to be able to look at all boolean inputs in a single while loop
		for (int i = 0; i < lettersRevealed.length; i++) {
			//if the inputted boolean at the given place is true, we add the letter of the word to that placement
			if (lettersRevealed[i]) {
				answer += word.charAt(i);
			//otherwise we add a _ or empty place
			} else {
				answer += "_";
			}
		}
		//prints the current word with the letters we have guessed seen
		System.out.println(answer);
	}
	
	//calls other methods and loops them to run game
	public static void runGame(String word){
		//initalizes the boolean of letters and if they are revealed or not
		boolean[] lettersRevealed = {false, false, false, false};
		final int guesses = 6;
		int place = 0;
		int misses = 0;
		java.util.Scanner reader = new java.util.Scanner(System.in);
		//while we don't have all the letters as true and we haven't run out of guesses
		while ( (!(lettersRevealed[0] && lettersRevealed[1] && lettersRevealed[2] && lettersRevealed[3])) && !(guesses == misses)){
			//asks user to input new letter
			System.out.println("Guess a letter:");
			//checks if the letter is in the word + the placement
			place = isLetterInWord(word, reader.next().charAt(0));
			//adds a miss if invalid place (-1 [less than 0])
			if (place < 0){
				misses++;
			} else {
				//changes the boolean of the given placement to true if it was there
				lettersRevealed[place] = true;
			}
			//prints the letter that have been guessed of the word in their correct placement
			printWord(word, lettersRevealed);
		}
		//if we ran out of guesses we lost, otherwise it means the while loop ended because all the letters were guessed, so we won!
		if (misses == guesses){
			System.out.println("You Lost.");
		} else {
			System.out.println("You Won!");
		}
	}
}